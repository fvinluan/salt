{% set jasper_user = 'tomcat' %}
{% set jasper_version = '6.0.1-bin' %}
{% set jasper_install_dir = ["/opt/jasperreports-server-", jasper_version]|join  %}
jasperserver:
  archive.extracted:
    - name: /opt/
    - source: salt://jasperserver/files/jasperreports-server-cp-{{ jasper_version }}.zip
    - source_hash: sha1=9208f0db26a462b8673540d369e237bbccdfc3d6
    - tar_options: v
    - archive_format: zip
    - user : {{ jasper_user }}
    - group : {{ jasper_user }}
    - if_missing: {{ jasper_install_dir }}
