rabbitmq-server:
  pkg.installed:
    - version: 3.4.3-1
    - skip_verify: true
    - sources:
      - rabbitmq-server: salt://rabbitmq/files/rabbitmq-server-3.4.3-1.noarch.rpm
  service:
    - running
    - enable: true
    - reload: true
    - watch:
      - pkg: rabbitmq-server

rabbitmq-bootstrap-plugins:
  rabbitmq_plugin.enabled:
    - names:
        - rabbitmq_management
        - rabbitmq_management_agent
        - rabbitmq_federation
        - rabbitmq_federation_management
    - require:
        - service: rabbitmq-server

guest:
  rabbitmq_user.absent:
    - require:
        - service: rabbitmq-server

admin:
  rabbitmq_user.present:
    - password: admin
    - force: true
    - tags: administrator
    - perms:
      - '/':
        - '.*'
        - '.*'
        - '.*'
    - require:
        - service: rabbitmq-server
