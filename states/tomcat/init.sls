{% set tomcat_user = 'tc' %}
{% set tomcat_version = '8.0.17' %}
{% set tomcat_install_dir = ["/opt/apache-tomcat-", tomcat_version]|join  %}
tomcat:
  archive.extracted:
    - name: /opt/
    - source: salt://tomcat/files/apache-tomcat-{{ tomcat_version }}.tar.gz
    - source_hash: md5=22c1b8952fe31dd2d87a03cc9d33483e
#    - tar_options: xvf
    - archive_format: tar
    - if_missing: {{ tomcat_install_dir }}
    - require:
        - user: {{ tomcat_user }}
  file.managed:
    - name: /etc/init.d/tomcat
    - source: salt://tomcat/files/tomcat
    - source_hash: md5=38479d5d543a7a2c43ee69a519a78b4f
    - user: root
    - group: root
    - mode: 755
    - template: jinja
    - context:
      tomcat_home: {{ tomcat_install_dir }}
      tomcat_user: {{ tomcat_user }}
  service.running:
    - enable: True
    - reload: True
    - watch:
      - file: {{ tomcat_install_dir }}/conf/server.xml

server_xml:
  file.managed:
    - name: {{ tomcat_install_dir }}/conf/server.xml
    - source: salt://tomcat/files/server.xml
    - source_hash: md5=8516c747d8881c8b0d4c2c170bdf9c1c
    - user: {{ tomcat_user }}
    - group: {{ tomcat_user }}
    - mode: 755
    - require:
        - archive: tomcat

remove-tomcat-default-files:
  file.absent:
    - names:
        - {{ tomcat_install_dir }}/webapps/examples/
        - {{ tomcat_install_dir }}/webapps/ROOT/
    - require:
        - archive: tomcat

install_oracle_jdbc_jar:
  file.managed:
    - name: {{ tomcat_install_dir }}/lib/ojdbc6-11.2.0.3.jar
    - source: salt://tomcat/files/ojdbc6-11.2.0.3.jar
    - source_hash: md5=6cd651cf8fc1cd203aa0b334cb69fbea
    - user: {{ tomcat_user }}
    - group: {{ tomcat_user }}
    - mode: 755
    - require:
        - archive: tomcat

install_mysql_jdbc_jar:
  file.managed:
    - name: {{ tomcat_install_dir }}/lib/mysql-connector-java-5.1.34-bin.jar
    - source: salt://tomcat/files/mysql-connector-java-5.1.34-bin.jar
    - source_hash: md5=adaa13571f32cfb67a388b6b0acfa8e8
    - user: {{ tomcat_user }}
    - group: {{ tomcat_user }}
    - mode: 755
    - require:
        - archive: tomcat

install_sqlserver_jdbc_jar:
  file.managed:
    - name: {{ tomcat_install_dir }}/lib/sqljdbc4-4.0.jar
    - source: salt://tomcat/files/sqljdbc4-4.0.jar
    - source_hash: md5=a99205071acbe2709103b971662992e0
    - user: {{ tomcat_user }}
    - group: {{ tomcat_user }}
    - mode: 755
    - require:
        - archive: tomcat

install_activation_jar:
  file.managed:
    - name: {{ tomcat_install_dir }}/lib/activation-1.1.1.jar
    - source: salt://tomcat/files/activation-1.1.1.jar
    - source_hash: md5=c264c7a20fe37c9dc702ab8b0cf54f58
    - user: {{ tomcat_user }}
    - group: {{ tomcat_user }}
    - mode: 755
    - require:
        - archive: tomcat

install_mail_jar:
  file.managed:
    - name: {{ tomcat_install_dir }}/lib/mail-1.4.7.jar
    - source: salt://tomcat/files/mail-1.4.7.jar
    - source_hash: md5=77f53ff0c78ba43c4812ecc9f53e20f8
    - user: {{ tomcat_user }}
    - group: {{ tomcat_user }}
    - mode: 755
    - require:
        - archive: tomcat

# apply directory / user permission
{{ tomcat_install_dir }}:
  file:
    - directory
    - user: {{ tomcat_user }}
    - group: {{ tomcat_user }}
    - dir_mode: 755
    - recurse:
        - user
        - group
