mysql:
  group:
    - present
    - system: True
  user: 
    - present
    - system: True
    - home: /var/lib/mysql
    - groups:
      - mysql
    - require:
      - group: mysql
  pkg:
    - installed
    - pkgs:
      - mysql-server
      - MySQL-python
  service:
    - running
    - name: mysqld
    - reload: True
    - enable: True
    - require:
      - pkg: mysql
      - user: mysql
    - watch:
      - file: /etc/my.cnf
  
#mysql_root:
#  mysql_user:
#    - present
#    - name: root
#    - password_hash: '*4ACFE3202A5FF5CF467898FC58AAB1D615029441'
#    - require:
#      - pkg: mysql
#
/etc/my.cnf:
  file.managed:
    - source: salt://mysql/files/my.cnf
    - user: root
    - group: root
    - mode: 644
    - template: jinja
    - context:
      mysql_db_home: /dbusr/data
      mysql_db_logs: /dbusr/log
      mysql_db_port: 3308
      mysql_db_socket: /var/lib/mysql/mysql.sock
# TODO : Use pillars