# watson agent application config


/appsettings/watson/log4j.properties:
  file.managed:
    - makedirs: true
    - source:
      - salt://apps/watson/files/agent/appsettings/log4j.properties

/appsettings/watson/rabbitmq.properties:
  file.managed:
    - makedirs: true
    - source:
      - salt://apps/watson/files/agent/appsettings/rabbitmq.properties

#rabbitmq config

rabbitmq_watson_virtual_host:
  rabbitmq_vhost.present:
      - name: watson
      - require:
          - service: rabbitmq-server
