# to create a password:
# python -c "import crypt; print crypt.crypt('', '\$6\$SALTsalt')"

#include:
#  - kerberos

{% set users = {
  'tc': {
    'uid': '501',
    'fullname': 'App Tomcat Server'
  },
  'jenkins': {
    'uid': '502',
    'fullname': 'App Jenkins',
    'password': '$6$SALTsalt$HcUiiK6Lxt66ky40N3gigo8QkCxQhKkIRH38KEEDDEFuxayiqIeS0clZyYkLJp1ao4kFkAvRRXBQfh5Kfhpnl0'
  }
} %}

{% for name, user in users.items() %}
{{ name }}:
  {% set shell = user.shell | default('/bin/bash') %}
  {% set state = user.state | default('present') %}
  {% set groups = user.groups %}
  user.{{ state }}:
    - fullname: {{ user.fullname }}
    - home: /home/{{ name }}
    - shell: {{ shell }}
    - uid: {{ user.uid }}
  {% if user.password is defined %}
    - password: {{ user.password }}
  {% endif %}
  {% if groups is defined %}
    - groups:
    {% for group in groups %}
      - {{ group }}
    {% endfor %}
  {% endif %}
 #   - require:
 #       - sls: kerberos
{% endfor %}

