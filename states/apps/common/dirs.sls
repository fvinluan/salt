# common server config used by servers regradless of app

/appsettings:
  file.directory:
    - user: tc
    - group: tc
    - dir_mode: 755
    - recurse:
        - user
        - group
    - require:
        - user: tc

/apptmp:
  file.directory:
    - use:
      - file: /apptmp

/applogs:
  file.directory:
    - use:
      - file: /appsettings
