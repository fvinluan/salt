# musterstation application config


/appsettings/musterstation/log4j.properties:
  file.managed:
    - makedirs: true
    - source:
      - salt://apps/musterstation/files/appsettings/log4j.properties

#mysql config
muster_station_db:
  mysql_database:
    - present
    - name: muster_station_db

muster_station_db_app_user:
  mysql_user:
    - present
    - host : localhost
    - name: ms_app
    - password_hash: "*62729C4E2C49E9E4DAC52877C90FBB46269099C6"
    - host: localhost

#select password('ms_app_admin');
muster_station_db_app_admin_user:
  mysql_user:
    - present
    - name: ms_app_admin
    - host: '%'
    - password_hash: "*106977672A5669B38903CCC1DF23B79FBA9AF472"
  mysql_grants:
    - present
    - database: muster_station_db.*
    - grant: ALL PRIVILEGES
    - user: ms_app_admin
    - host: 10.1.1.1
