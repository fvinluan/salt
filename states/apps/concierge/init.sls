# concierge application config

/appsettings/concierge/log4j.properties:
  file.managed:
    - makedirs: true
    - source:
      - salt://apps/concierge/files/appsettings/log4j.properties

#mysql config
concierge_db:
  mysql_database:
    - present
    - name: concierge_db

#select password('concierge_app');
concierge_db_app_user:
  mysql_user:
    - present
    - name: concierge_app
    - host: '%'
    - password_hash: "*6B51B0818966C9DA712CAE400DB3D6889A02A6F4"
  mysql_grants:
    - present
    - database: concierge_db.*
    - grant: SELECT,INSERT,UPDATE,DELETE
    - user: concierge_app
    - host: 10.1.1.1

#select password('con_app_admin');
concierge_db_app_admin_user:
  mysql_user:
    - present
    - name: con_app_admin
    - host: '%'
    - password_hash: "*D37FC60D2D5A32ABDE098A563841592D8AC33A00"
  mysql_grants:
    - present
    - database: concierge_db.*
    - grant: ALL PRIVILEGES
    - user: con_app_admin
    - host: 10.1.1.1
