base:
  '*':
    - apps/common.users
    - apps/common.dirs

  '*watsonhq*':
    - java
    - httpd
    - tomcat
    - rabbitmq
    - mongodb

  '*watsonargent*':
    - java
    - httpd
    - tomcat
    - rabbitmq

  '*musterstation*':
    - mysql
    - java
    - httpd
    - tomcat
    - apps/musterstation

  '*concierge*':
    - mysql
    - java
    - httpd
    - tomcat
    - apps/concierge

  '*jasperserver*':
    - mysql
    - java
    - httpd
    - tomcat
    - jasperserver
