httpd:
  pkg.installed: []
  service.running:
    - enable: true
    - reload: true
    - watch:
      - file: /etc/httpd/conf/httpd.conf

/etc/httpd/conf/httpd.conf:
    file.managed:
        - source: salt://httpd/files/httpd.conf

